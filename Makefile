classpath = ".:../../MineLibrary.jar:../resources/org.eclipse.paho.client.mqttv3-1.1.0.jar:../resources/snakeyaml-1.25.jar:../resources/commons-cli-1.4.jar"

build: clean
	cd build; javac -d . -classpath $(classpath) ../src/org/tojo/mine2mqtt/*.java

run:
	cd build; java -classpath $(classpath) org.tojo.mine2mqtt.Server

.PHONY: clean

clean:
	rm -fr build/*
