/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tojo.mine2mqtt;

import com.monnit.mine.BaseApplication.ExtensionMethods.Parameter;
import com.monnit.mine.MonnitMineAPI.GatewayMessage;
import com.monnit.mine.MonnitMineAPI.iGatewayMessageHandler;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author tojo
 */
public class GatewayMessageHandler implements iGatewayMessageHandler {
    
    @Override
    public void ProcessGatewayMessage(GatewayMessage gatewayMessage) throws Exception {
        MqttClient client = Server.getMqttClient();
        String topicPrefix = "node/monnitlink-ethernet-gateway/gateway/" + gatewayMessage.getGatewayID() + "/";
        for (Parameter item : gatewayMessage.getGatewayParameterList()) {
            MqttMessage message = new MqttMessage();
            String topic = topicPrefix + item.getKeyName().toLowerCase();
            String payload = String.valueOf(item.getKeyValue());
            message.setPayload(payload.getBytes());                     
            client.publish(topic, message);
        }
        Server.print(gatewayMessage.toString());
        //System.out.println(gatewayMessage.toString());
            
    }
}