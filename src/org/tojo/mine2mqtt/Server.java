/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tojo.mine2mqtt;

import com.monnit.mine.MonnitMineAPI.Gateway;
import com.monnit.mine.MonnitMineAPI.MineServer;
import com.monnit.mine.MonnitMineAPI.Sensor;
import com.monnit.mine.MonnitMineAPI.enums.eFirmwareGeneration;
import com.monnit.mine.MonnitMineAPI.enums.eGatewayType;
import com.monnit.mine.MonnitMineAPI.enums.eMineListenerProtocol;
import com.monnit.mine.MonnitMineAPI.enums.eSensorApplication;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.yaml.snakeyaml.Yaml;
import org.apache.commons.cli.*;
import org.apache.commons.cli.CommandLineParser;

public class Server {

    protected static MqttClient mqttClientInstance;
    protected static MyGateway configData;
    
    public static HashMap<Long, String> sensorAlias;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO: Do we need commandline parsing if run through docker only?
            Options options = new Options();
            Option help = new Option( "help", "print this message" );
            Option sensorsfile = OptionBuilder.withArgName( "yaml file" )
                                              .hasArg()
                                              .withDescription( "use given file for sensors" )
                                              .create("sensors");
            Option port = OptionBuilder.withArgName( "port" )
                                              .hasArg()
                                              .withDescription( "use port (default 3000)" )
                                              .create("port");

            options.addOption(help);
            options.addOption(sensorsfile);
            options.addOption(port);

            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);
            HelpFormatter formatter = new HelpFormatter();

            if (cmd.hasOption("help")) {
                formatter.printHelp( "mine2mqtt", options );
                System.exit(0);
            }

            int bindToPort = 3000;
            if (cmd.hasOption("port")) {
                bindToPort = Integer.parseInt(cmd.getOptionValue("port"));
            }
        
            sensorAlias = new HashMap<Long, String>();
        
            Yaml yaml = new Yaml();
            
            print("Reading configuration");
            String document = readFile("sensors.yml", StandardCharsets.US_ASCII);
            MyGateway foo = yaml.loadAs(document, MyGateway.class);
            configData = foo;
            print(configData.sensors.length + " sensors available");
            MqttClient _mqttClient = getMqttClient();

            eGatewayType bar = eGatewayType.valueOf(foo.type);
            
            MineServer _server = new MineServer(eMineListenerProtocol.TCPAndUDP, InetAddress.getByName("0.0.0.0"), bindToPort);
            Gateway _gateway = new Gateway(foo.id, bar);
            _server.RegisterGateway(_gateway);
            print("Gateway " + foo.id + "/" + bar + " registered");

            _server.addGatewayDataProcessingHandler(new GatewayMessageHandler());
            _server.addSensorDataProcessingHandler(new SensorMessageHandler());

            registerSensors(_server, _gateway, foo.sensors);

            _server.StartServer();
            print("Server started");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
    
    public static synchronized void print(String s) {
        System.out.println(s);
    }
    
    public static MqttClient getMqttClient() throws Exception {
        if (mqttClientInstance == null) {
            mqttClientInstance = new MqttClient(configData.mqtt.broker, MqttClient.generateClientId());
            mqttClientInstance.connect();
            print("MQTT Client connected to " + configData.mqtt.broker);
        }
        if (! mqttClientInstance.isConnected()) {
            mqttClientInstance.reconnect();
        }
        return mqttClientInstance;
    }
    
    protected static void registerSensors(MineServer _server, Gateway _gateway, MySensor[] sensors) throws Exception {
        for (int i = 0; i < sensors.length; i++) {
            MySensor s = sensors[i];
            eSensorApplication sType = eSensorApplication.valueOf(s.type);
            eFirmwareGeneration sGen = eFirmwareGeneration.valueOf(s.gen);
            Sensor m = new Sensor(s.id, sType, "1.0.0", sGen);
            _server.RegisterSensor(_gateway.getGatewayID(), m);
            sensorAlias.put(s.id, s.alias);
            print("Sensor " + s.id + "/" + s.type + " (" + s.gen + ") registered. Alias: " + s.alias);
        }
    }
}
