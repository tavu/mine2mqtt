/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tojo.mine2mqtt;

import com.monnit.mine.BaseApplication.Datum;
import java.util.List;

import com.monnit.mine.MonnitMineAPI.Gateway;
import com.monnit.mine.MonnitMineAPI.SensorMessage;
import com.monnit.mine.MonnitMineAPI.iSensorMessageHandler;
import java.util.Arrays;
import java.util.stream.Collectors;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class SensorMessageHandler implements iSensorMessageHandler {

    @Override
    public void ProcessSensorMessages(List<SensorMessage> sensorMessageList, Gateway gateway) throws Exception {
        for (SensorMessage msg : sensorMessageList) {
            if (msg.ProfileID > 65000) {
                //Sensor sens = GUIListenerFunctions.FindSensorBySensorID(msg.SensorID);
                //msg.ProfileID = sens.MonnitApplication.Value();
            }
            //System.out.println(msg.toString());
            MqttClient client = Server.getMqttClient();
            
            MqttMessage message = new MqttMessage();
            String topic = Server.sensorAlias.get(msg.SensorID) + "voltage";
            String[] topicSplit = topic.split("/");
            topicSplit[2] = "battery";
            topic = Arrays.stream(topicSplit).collect(Collectors.joining("/"));
            String payload = String.valueOf(msg.getVoltage());
            message.setPayload(payload.getBytes());                     
            client.publish(topic, message);

            MqttMessage message2 = new MqttMessage();
            String topic2 = Server.sensorAlias.get(msg.SensorID) + "signal";
            String payload2 = String.valueOf(msg.SignalStrengthPercent());
            message2.setPayload(payload2.getBytes());                     
            client.publish(topic2, message2);
            
            List<Datum> datums = msg.getData();
            for (Datum d : datums) {
                MqttMessage message3 = new MqttMessage();
                String topic3 = Server.sensorAlias.get(msg.SensorID) + "" + d.Description.toLowerCase();
                String payload3 = String.valueOf(d.Data);
                message3.setPayload(payload3.getBytes());                     
                client.publish(topic3, message3);
            }
        }
    }

}
