FROM openjdk:8-jdk-slim
RUN apt update; apt install -yy make curl unzip 
RUN cd /tmp;\
 curl -O http://mine.imonnit.com/Java/v3.0.0/Monnit_Mine_Java_3.0.0.2.zip;\
 unzip Monnit_Mine_Java_3.0.0.2.zip JavaMineLibrary/MineLibrary.jar;\
 mv JavaMineLibrary/MineLibrary.jar /
COPY resources/*.jar /
COPY . /sources
WORKDIR /sources
RUN make build
ENTRYPOINT ["bin/entrypoint"] 
# COMMAND 
